package com.mod.solidores;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.block.Block;
import net.minecraft.creativetab.CreativeTabs;

public class Blocks {
    public static Block STONE_BLOCK;

    public static void init(CreativeTabs tab) {
        STONE_BLOCK = new StoneBlock(tab);
        GameRegistry.registerBlock(STONE_BLOCK, StoneBlock.NAME);
    }
}

