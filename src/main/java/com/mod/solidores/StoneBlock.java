package com.mod.solidores;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;

import java.util.Random;

public class StoneBlock extends Block {
    public static final String NAME = "StoneBlock";

    public StoneBlock(CreativeTabs tab) {
        super(Material.rock);

        super.setBlockName(NAME);
        super.setCreativeTab(tab);
        this.setHardness(2F);
        this.setResistance(15F);
        this.setStepSound(Block.soundTypeStone);
        this.setLightLevel(0.5F);
        this.setHarvestLevel("shovel",0);
        this.setBlockTextureName(SolidOresMod.MODID + ":bug");
    }
}



