package com.mod.solidores;

import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.init.Items;
import net.minecraft.item.Item;

@Mod(modid = SolidOresMod.MODID, version = SolidOresMod.VERSION)
public class SolidOresMod
{
    public static final String MODID = "solidores";
    public static final String VERSION = "1.0";
    private static final String TAB_NAME = "tabSolidOres";

    public static CreativeTabs tabCustom = new CreativeTabs(TAB_NAME) {
        @Override
        @SideOnly(Side.CLIENT)
        public Item getTabIconItem() {
            return Items.ender_eye;
        }
    };

    @EventHandler
    public void init(FMLInitializationEvent event) {
        Blocks.init(tabCustom);
    }
}

